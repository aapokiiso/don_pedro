Don Pedro
=========
The best food spy ever. Professionally trained to deliver you latest menus form nearby restaurants.
Currently lacks of other features (see info below).

Feature reward program
----------------------
Due to that project is in very early stage and it lacks of many features, pioneer _Feature reward program_ was created.
That means **you will receive beer for every approved feature you will implement.**
It includes new _menu scraplers_, actions, style or code improvements and possible many others.


Deployment
----------

### Environment variables
    export SLACK_API_TOKEN=
    export SLACK_CHANNEL=

### Running in docker
     python3 setup.py dockerize deploy

### Continuous integration
    python3 setup.py dockerize_ci deploy_ci

### Setting up environment in Google Cloud

    export CLOUD_PROJECT=
    export CLOUD_MACHINE=

    gcloud compute \
        --project=$CLOUD_PROJECT \
        instances create $CLOUD_MACHINE \
        --zone=us-east1-b --machine-type=f1-micro \
        --subnet=default --network-tier=PREMIUM \
        --no-service-account --no-scopes \
        --image=ubuntu-1804-bionic-v20180723 --image-project=ubuntu-os-cloud \
        --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=$CLOUD_MACHINE

    gcloud compute \
        --project=$CLOUD_PROJECT \
        addresses create $CLOUD_MACHINE \
        --region us-east1 \
        --addresses $(gcloud compute instances describe \
            $CLOUD_MACHINE \
            --zone us-east1-b \
            --format json \
            | jq -r .networkInterfaces[0].accessConfigs[0].natIP)

    docker-machine create \
        --driver google \
        --google-project $CLOUD_PROJECT \
        --google-zone us-east1-b \
        --google-use-existing $CLOUD_MACHINE
        
    eval (docker-machine env $CLOUD_MACHINE)
