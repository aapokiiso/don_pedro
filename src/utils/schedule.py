from datetime import datetime, timezone
from threading import Timer


def schedule(h: int, m: int, task) -> None:
	x = datetime.now(timezone.utc)
	y = x.replace(day=x.day + 1, hour=h, minute=m, second=0, microsecond=0)

	def wrapper():
		task()
		schedule(h, m, task)

	Timer((y - x).seconds + 1, wrapper).start()
