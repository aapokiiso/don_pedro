from py_translator import Translator


def translate(text: str) -> str:
	try:
		return Translator().translate(text, dest='en', src='fi').text
	except:
		return ''
