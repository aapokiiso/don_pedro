def itemize(text: str) -> str:
	return '\n'.join(['• ' + line for line in text.split('\n')])
