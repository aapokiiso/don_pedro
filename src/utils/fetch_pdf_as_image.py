from urllib.request import urlopen
from wand.image import Image

Image.load = lambda self: None
Image.tobytes = lambda self, *args: self.make_blob('RGB') if args == ('raw', 'RGB') else None


def fetch_pdf_as_image(url: str, dpi: int = 300) -> Image:
	return Image(
		file=urlopen(url),
		format='pdf',
		resolution=dpi,
		depth=8
	)
