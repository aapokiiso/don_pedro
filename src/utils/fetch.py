from lxml import etree
from urllib.request import urlopen


def fetch(url: str, path: str) -> str:
	return ''.join(etree.parse(urlopen(url), etree.HTMLParser()).xpath(path))
