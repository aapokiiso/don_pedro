def italize(text: str) -> str:
	return '\n'.join(['_{}_'.format(line) for line in text.split('\n')])
