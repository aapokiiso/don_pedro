from ..utils.fetch import fetch
from ..utils.fetch_pdf_as_image import fetch_pdf_as_image
from urllib.parse import urlparse
from datetime import datetime, timezone
from pyocr.libtesseract import image_to_string
from pyocr.builders import TextBuilder

URL = 'http://www.thaimaalainenravintola.fi/kamppi'
PATH = '//a[contains(text(),"LOUNASLISTA {}")]/@href'
DAYS = ['MA', 'TI', 'KE', 'TO', 'PE']


def orchid():
	menu = fetch(URL, PATH.format(DAYS[datetime.now(timezone.utc).weekday()]))
	menu_url = '{0.scheme}://{0.netloc}/{1}'.format(urlparse(URL), menu)
	pdf = fetch_pdf_as_image(menu_url)

	width, height = pdf.size
	pdf.crop(
		left=int(.11 * width),
		top=int(height * .265),
		right=int(.89 * width),
		bottom=int(height * .74),
	)

	txt = image_to_string(
		pdf,
		lang='eng+fin+tha',
		builder=TextBuilder()
	)

	return {
		'title': 'Thai orchid lounas menu',
		'title_link': menu_url,
		'text': txt
	}
