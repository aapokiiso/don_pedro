from ..utils.translate import translate
from ..utils.fetch import fetch
from ..utils.italize import italize
from datetime import datetime, timezone

URL = 'https://www.pizzahut.fi/menu/#lounas'
PATCH = '//div[@class="product-list"]/p[contains(text(),"{}:")]/text()'
DAYS = ['Ma', 'Ti', 'Ke', 'To', 'Pe']


def hut():
	menu_fi = fetch(URL, PATCH.format(DAYS[datetime.now(timezone.utc).weekday()]))
	menu_en = translate(menu_fi)
	return {
		'title': 'Pizza hut lounas menu',
		'title_link': URL,
		'text': '{}\n\n{}'.format(menu_fi, italize(menu_en))
	}
